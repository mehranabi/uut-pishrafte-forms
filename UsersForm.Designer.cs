﻿namespace FormApps
{
    partial class UsersForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.first_name_input = new System.Windows.Forms.TextBox();
            this.last_name_input = new System.Windows.Forms.TextBox();
            this.create_btn = new System.Windows.Forms.Button();
            this.users_combo = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.initial_list = new System.Windows.Forms.ListBox();
            this.add_to_initial_list_btn = new System.Windows.Forms.Button();
            this.add_all_final = new System.Windows.Forms.Button();
            this.add_one_final = new System.Windows.Forms.Button();
            this.take_back_one = new System.Windows.Forms.Button();
            this.take_back_all = new System.Windows.Forms.Button();
            this.final_list = new System.Windows.Forms.ListBox();
            this.label4 = new System.Windows.Forms.Label();
            this.enable_checkbox = new System.Windows.Forms.CheckBox();
            this.show_list_chckbx = new System.Windows.Forms.GroupBox();
            this.show_form_no = new System.Windows.Forms.RadioButton();
            this.show_form_yes = new System.Windows.Forms.RadioButton();
            this.show_list_chckbx.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "First Name:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 77);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(112, 25);
            this.label2.TabIndex = 1;
            this.label2.Text = "Last Name:";
            // 
            // first_name_input
            // 
            this.first_name_input.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.first_name_input.Location = new System.Drawing.Point(130, 12);
            this.first_name_input.Name = "first_name_input";
            this.first_name_input.Size = new System.Drawing.Size(158, 34);
            this.first_name_input.TabIndex = 2;
            // 
            // last_name_input
            // 
            this.last_name_input.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.last_name_input.Location = new System.Drawing.Point(130, 71);
            this.last_name_input.Name = "last_name_input";
            this.last_name_input.Size = new System.Drawing.Size(158, 34);
            this.last_name_input.TabIndex = 3;
            // 
            // create_btn
            // 
            this.create_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.create_btn.Location = new System.Drawing.Point(17, 125);
            this.create_btn.Name = "create_btn";
            this.create_btn.Size = new System.Drawing.Size(107, 45);
            this.create_btn.TabIndex = 4;
            this.create_btn.Text = "Add";
            this.create_btn.UseVisualStyleBackColor = true;
            this.create_btn.Click += new System.EventHandler(this.create_btn_Click);
            // 
            // users_combo
            // 
            this.users_combo.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.users_combo.FormattingEnabled = true;
            this.users_combo.Location = new System.Drawing.Point(12, 233);
            this.users_combo.Name = "users_combo";
            this.users_combo.Size = new System.Drawing.Size(276, 39);
            this.users_combo.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 201);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 29);
            this.label3.TabIndex = 6;
            this.label3.Text = "Users:";
            // 
            // initial_list
            // 
            this.initial_list.FormattingEnabled = true;
            this.initial_list.ItemHeight = 16;
            this.initial_list.Location = new System.Drawing.Point(411, 12);
            this.initial_list.Name = "initial_list";
            this.initial_list.Size = new System.Drawing.Size(192, 260);
            this.initial_list.TabIndex = 7;
            // 
            // add_to_initial_list_btn
            // 
            this.add_to_initial_list_btn.Location = new System.Drawing.Point(317, 233);
            this.add_to_initial_list_btn.Name = "add_to_initial_list_btn";
            this.add_to_initial_list_btn.Size = new System.Drawing.Size(62, 39);
            this.add_to_initial_list_btn.TabIndex = 8;
            this.add_to_initial_list_btn.Text = ">>";
            this.add_to_initial_list_btn.UseVisualStyleBackColor = true;
            this.add_to_initial_list_btn.Click += new System.EventHandler(this.add_to_initial_list_btn_Click);
            // 
            // add_all_final
            // 
            this.add_all_final.Location = new System.Drawing.Point(622, 12);
            this.add_all_final.Name = "add_all_final";
            this.add_all_final.Size = new System.Drawing.Size(62, 39);
            this.add_all_final.TabIndex = 9;
            this.add_all_final.Text = ">>";
            this.add_all_final.UseVisualStyleBackColor = true;
            this.add_all_final.Click += new System.EventHandler(this.add_all_final_Click);
            // 
            // add_one_final
            // 
            this.add_one_final.Location = new System.Drawing.Point(622, 57);
            this.add_one_final.Name = "add_one_final";
            this.add_one_final.Size = new System.Drawing.Size(62, 39);
            this.add_one_final.TabIndex = 10;
            this.add_one_final.Text = ">";
            this.add_one_final.UseVisualStyleBackColor = true;
            this.add_one_final.Click += new System.EventHandler(this.add_one_final_Click);
            // 
            // take_back_one
            // 
            this.take_back_one.Location = new System.Drawing.Point(622, 233);
            this.take_back_one.Name = "take_back_one";
            this.take_back_one.Size = new System.Drawing.Size(62, 39);
            this.take_back_one.TabIndex = 11;
            this.take_back_one.Text = "<";
            this.take_back_one.UseVisualStyleBackColor = true;
            this.take_back_one.Click += new System.EventHandler(this.take_back_one_Click);
            // 
            // take_back_all
            // 
            this.take_back_all.Location = new System.Drawing.Point(622, 188);
            this.take_back_all.Name = "take_back_all";
            this.take_back_all.Size = new System.Drawing.Size(62, 39);
            this.take_back_all.TabIndex = 12;
            this.take_back_all.Text = "<<";
            this.take_back_all.UseVisualStyleBackColor = true;
            this.take_back_all.Click += new System.EventHandler(this.take_back_all_Click);
            // 
            // final_list
            // 
            this.final_list.FormattingEnabled = true;
            this.final_list.ItemHeight = 16;
            this.final_list.Location = new System.Drawing.Point(701, 12);
            this.final_list.Name = "final_list";
            this.final_list.Size = new System.Drawing.Size(192, 260);
            this.final_list.TabIndex = 13;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 286);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(213, 17);
            this.label4.TabIndex = 14;
            this.label4.Text = "Developed by Mehran Abghari :)";
            // 
            // enable_checkbox
            // 
            this.enable_checkbox.AutoSize = true;
            this.enable_checkbox.Checked = true;
            this.enable_checkbox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.enable_checkbox.Location = new System.Drawing.Point(916, 188);
            this.enable_checkbox.Name = "enable_checkbox";
            this.enable_checkbox.Size = new System.Drawing.Size(125, 21);
            this.enable_checkbox.TabIndex = 15;
            this.enable_checkbox.Text = "Enable buttons";
            this.enable_checkbox.UseVisualStyleBackColor = true;
            this.enable_checkbox.CheckedChanged += new System.EventHandler(this.enable_checkbox_CheckedChanged);
            // 
            // show_list_chckbx
            // 
            this.show_list_chckbx.Controls.Add(this.show_form_no);
            this.show_list_chckbx.Controls.Add(this.show_form_yes);
            this.show_list_chckbx.Location = new System.Drawing.Point(916, 48);
            this.show_list_chckbx.Name = "show_list_chckbx";
            this.show_list_chckbx.Size = new System.Drawing.Size(130, 102);
            this.show_list_chckbx.TabIndex = 17;
            this.show_list_chckbx.TabStop = false;
            this.show_list_chckbx.Text = "Show Final List?";
            // 
            // show_form_no
            // 
            this.show_form_no.AutoSize = true;
            this.show_form_no.Location = new System.Drawing.Point(6, 60);
            this.show_form_no.Name = "show_form_no";
            this.show_form_no.Size = new System.Drawing.Size(47, 21);
            this.show_form_no.TabIndex = 1;
            this.show_form_no.Text = "No";
            this.show_form_no.UseVisualStyleBackColor = true;
            this.show_form_no.CheckedChanged += new System.EventHandler(this.show_form_no_CheckedChanged);
            // 
            // show_form_yes
            // 
            this.show_form_yes.AutoSize = true;
            this.show_form_yes.Checked = true;
            this.show_form_yes.Location = new System.Drawing.Point(6, 33);
            this.show_form_yes.Name = "show_form_yes";
            this.show_form_yes.Size = new System.Drawing.Size(53, 21);
            this.show_form_yes.TabIndex = 0;
            this.show_form_yes.TabStop = true;
            this.show_form_yes.Text = "Yes";
            this.show_form_yes.UseVisualStyleBackColor = true;
            this.show_form_yes.CheckedChanged += new System.EventHandler(this.show_form_yes_CheckedChanged);
            // 
            // UsersForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1059, 312);
            this.Controls.Add(this.show_list_chckbx);
            this.Controls.Add(this.enable_checkbox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.final_list);
            this.Controls.Add(this.take_back_all);
            this.Controls.Add(this.take_back_one);
            this.Controls.Add(this.add_one_final);
            this.Controls.Add(this.add_all_final);
            this.Controls.Add(this.add_to_initial_list_btn);
            this.Controls.Add(this.initial_list);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.users_combo);
            this.Controls.Add(this.create_btn);
            this.Controls.Add(this.last_name_input);
            this.Controls.Add(this.first_name_input);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "UsersForm";
            this.Text = "Users Form";
            this.show_list_chckbx.ResumeLayout(false);
            this.show_list_chckbx.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox first_name_input;
        private System.Windows.Forms.TextBox last_name_input;
        private System.Windows.Forms.Button create_btn;
        private System.Windows.Forms.ComboBox users_combo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListBox initial_list;
        private System.Windows.Forms.Button add_to_initial_list_btn;
        private System.Windows.Forms.Button add_all_final;
        private System.Windows.Forms.Button add_one_final;
        private System.Windows.Forms.Button take_back_one;
        private System.Windows.Forms.Button take_back_all;
        private System.Windows.Forms.ListBox final_list;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox enable_checkbox;
        private System.Windows.Forms.GroupBox show_list_chckbx;
        private System.Windows.Forms.RadioButton show_form_no;
        private System.Windows.Forms.RadioButton show_form_yes;
    }
}