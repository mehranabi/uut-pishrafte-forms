﻿namespace FormApps
{
    partial class Colorize
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.red_scroll = new System.Windows.Forms.VScrollBar();
            this.green_scroll = new System.Windows.Forms.VScrollBar();
            this.blue_scroll = new System.Windows.Forms.VScrollBar();
            this.SuspendLayout();
            // 
            // red_scroll
            // 
            this.red_scroll.Location = new System.Drawing.Point(9, 9);
            this.red_scroll.Maximum = 255;
            this.red_scroll.Name = "red_scroll";
            this.red_scroll.Size = new System.Drawing.Size(51, 190);
            this.red_scroll.TabIndex = 0;
            this.red_scroll.Scroll += new System.Windows.Forms.ScrollEventHandler(this.red_scroll_Scroll);
            // 
            // green_scroll
            // 
            this.green_scroll.Location = new System.Drawing.Point(69, 9);
            this.green_scroll.Maximum = 255;
            this.green_scroll.Name = "green_scroll";
            this.green_scroll.Size = new System.Drawing.Size(47, 190);
            this.green_scroll.TabIndex = 1;
            this.green_scroll.Scroll += new System.Windows.Forms.ScrollEventHandler(this.green_scroll_Scroll);
            // 
            // blue_scroll
            // 
            this.blue_scroll.Location = new System.Drawing.Point(125, 9);
            this.blue_scroll.Maximum = 255;
            this.blue_scroll.Name = "blue_scroll";
            this.blue_scroll.Size = new System.Drawing.Size(46, 190);
            this.blue_scroll.TabIndex = 2;
            this.blue_scroll.Scroll += new System.Windows.Forms.ScrollEventHandler(this.blue_scroll_Scroll);
            // 
            // Colorize
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(604, 430);
            this.Controls.Add(this.blue_scroll);
            this.Controls.Add(this.green_scroll);
            this.Controls.Add(this.red_scroll);
            this.Name = "Colorize";
            this.Text = "Colorize";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.VScrollBar red_scroll;
        private System.Windows.Forms.VScrollBar green_scroll;
        private System.Windows.Forms.VScrollBar blue_scroll;
    }
}