﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FormApps
{
    public partial class Colorize : Form
    {
        private int red = 0;
        private int green = 0;
        private int blue = 0;

        public Colorize()
        {
            InitializeComponent();
        }

        private void red_scroll_Scroll(object sender, ScrollEventArgs e)
        {
            red = red_scroll.Value;
            applyColor();
        }

        private void green_scroll_Scroll(object sender, ScrollEventArgs e)
        {
            green = green_scroll.Value;
            applyColor();
        }

        private void blue_scroll_Scroll(object sender, ScrollEventArgs e)
        {
            blue = blue_scroll.Value;
            applyColor();
        }

        private void applyColor()
        {
            this.BackColor = Color.FromArgb(red, green, blue);
        }
    }
}
