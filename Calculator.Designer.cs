﻿namespace FormApps
{
    partial class Calculator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.input = new System.Windows.Forms.TextBox();
            this.clear_btn = new System.Windows.Forms.Button();
            this.clear_current_btn = new System.Windows.Forms.Button();
            this.divide_btn = new System.Windows.Forms.Button();
            this.subs_btn = new System.Windows.Forms.Button();
            this.mult_btn = new System.Windows.Forms.Button();
            this.nine_btn = new System.Windows.Forms.Button();
            this.eight_btn = new System.Windows.Forms.Button();
            this.seven_btn = new System.Windows.Forms.Button();
            this.calc_btn = new System.Windows.Forms.Button();
            this.six_btn = new System.Windows.Forms.Button();
            this.five_btn = new System.Windows.Forms.Button();
            this.four_btn = new System.Windows.Forms.Button();
            this.three_btn = new System.Windows.Forms.Button();
            this.two_btn = new System.Windows.Forms.Button();
            this.one_btn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.zero_btn = new System.Windows.Forms.Button();
            this.dot_btn = new System.Windows.Forms.Button();
            this.sum_btn = new System.Windows.Forms.Button();
            this.no1_label = new System.Windows.Forms.Label();
            this.operator_label = new System.Windows.Forms.Label();
            this.no2_label = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // input
            // 
            this.input.AcceptsTab = true;
            this.input.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.input.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.input.Location = new System.Drawing.Point(11, 33);
            this.input.Name = "input";
            this.input.Size = new System.Drawing.Size(319, 75);
            this.input.TabIndex = 0;
            this.input.Text = "0";
            this.input.TextChanged += new System.EventHandler(this.input_TextChanged);
            // 
            // clear_btn
            // 
            this.clear_btn.BackColor = System.Drawing.Color.Crimson;
            this.clear_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clear_btn.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.clear_btn.Location = new System.Drawing.Point(12, 114);
            this.clear_btn.Name = "clear_btn";
            this.clear_btn.Size = new System.Drawing.Size(75, 67);
            this.clear_btn.TabIndex = 1;
            this.clear_btn.Text = "C";
            this.clear_btn.UseVisualStyleBackColor = false;
            this.clear_btn.Click += new System.EventHandler(this.clear_btn_Click);
            // 
            // clear_current_btn
            // 
            this.clear_current_btn.BackColor = System.Drawing.Color.Crimson;
            this.clear_current_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clear_current_btn.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.clear_current_btn.Location = new System.Drawing.Point(93, 114);
            this.clear_current_btn.Name = "clear_current_btn";
            this.clear_current_btn.Size = new System.Drawing.Size(75, 67);
            this.clear_current_btn.TabIndex = 2;
            this.clear_current_btn.Text = "CE";
            this.clear_current_btn.UseVisualStyleBackColor = false;
            this.clear_current_btn.Click += new System.EventHandler(this.clear_current_btn_Click);
            // 
            // divide_btn
            // 
            this.divide_btn.BackColor = System.Drawing.Color.Lime;
            this.divide_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.divide_btn.ForeColor = System.Drawing.SystemColors.ControlText;
            this.divide_btn.Location = new System.Drawing.Point(254, 116);
            this.divide_btn.Name = "divide_btn";
            this.divide_btn.Size = new System.Drawing.Size(75, 67);
            this.divide_btn.TabIndex = 3;
            this.divide_btn.Text = "÷";
            this.divide_btn.UseVisualStyleBackColor = false;
            this.divide_btn.Click += new System.EventHandler(this.divide_btn_Click);
            // 
            // subs_btn
            // 
            this.subs_btn.BackColor = System.Drawing.Color.Lime;
            this.subs_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.subs_btn.ForeColor = System.Drawing.SystemColors.ControlText;
            this.subs_btn.Location = new System.Drawing.Point(255, 259);
            this.subs_btn.Name = "subs_btn";
            this.subs_btn.Size = new System.Drawing.Size(75, 67);
            this.subs_btn.TabIndex = 4;
            this.subs_btn.Text = "−";
            this.subs_btn.UseVisualStyleBackColor = false;
            this.subs_btn.Click += new System.EventHandler(this.subs_btn_Click);
            // 
            // mult_btn
            // 
            this.mult_btn.BackColor = System.Drawing.Color.Lime;
            this.mult_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mult_btn.ForeColor = System.Drawing.SystemColors.ControlText;
            this.mult_btn.Location = new System.Drawing.Point(174, 116);
            this.mult_btn.Name = "mult_btn";
            this.mult_btn.Size = new System.Drawing.Size(75, 67);
            this.mult_btn.TabIndex = 8;
            this.mult_btn.Text = "×";
            this.mult_btn.UseVisualStyleBackColor = false;
            this.mult_btn.Click += new System.EventHandler(this.mult_btn_Click);
            // 
            // nine_btn
            // 
            this.nine_btn.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.nine_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nine_btn.Location = new System.Drawing.Point(174, 187);
            this.nine_btn.Name = "nine_btn";
            this.nine_btn.Size = new System.Drawing.Size(75, 67);
            this.nine_btn.TabIndex = 7;
            this.nine_btn.Text = "9";
            this.nine_btn.UseVisualStyleBackColor = false;
            this.nine_btn.Click += new System.EventHandler(this.nine_btn_Click);
            // 
            // eight_btn
            // 
            this.eight_btn.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.eight_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.eight_btn.Location = new System.Drawing.Point(93, 187);
            this.eight_btn.Name = "eight_btn";
            this.eight_btn.Size = new System.Drawing.Size(75, 67);
            this.eight_btn.TabIndex = 6;
            this.eight_btn.Text = "8";
            this.eight_btn.UseVisualStyleBackColor = false;
            this.eight_btn.Click += new System.EventHandler(this.eight_btn_Click);
            // 
            // seven_btn
            // 
            this.seven_btn.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.seven_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.seven_btn.Location = new System.Drawing.Point(12, 187);
            this.seven_btn.Name = "seven_btn";
            this.seven_btn.Size = new System.Drawing.Size(75, 67);
            this.seven_btn.TabIndex = 5;
            this.seven_btn.Text = "7";
            this.seven_btn.UseVisualStyleBackColor = false;
            this.seven_btn.Click += new System.EventHandler(this.seven_btn_Click);
            // 
            // calc_btn
            // 
            this.calc_btn.BackColor = System.Drawing.Color.Lime;
            this.calc_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.calc_btn.ForeColor = System.Drawing.SystemColors.ControlText;
            this.calc_btn.Location = new System.Drawing.Point(255, 333);
            this.calc_btn.Name = "calc_btn";
            this.calc_btn.Size = new System.Drawing.Size(75, 140);
            this.calc_btn.TabIndex = 12;
            this.calc_btn.Text = "=";
            this.calc_btn.UseVisualStyleBackColor = false;
            this.calc_btn.Click += new System.EventHandler(this.clac_btn_Click);
            // 
            // six_btn
            // 
            this.six_btn.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.six_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.six_btn.Location = new System.Drawing.Point(174, 260);
            this.six_btn.Name = "six_btn";
            this.six_btn.Size = new System.Drawing.Size(75, 67);
            this.six_btn.TabIndex = 11;
            this.six_btn.Text = "6";
            this.six_btn.UseVisualStyleBackColor = false;
            this.six_btn.Click += new System.EventHandler(this.six_btn_Click);
            // 
            // five_btn
            // 
            this.five_btn.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.five_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.five_btn.Location = new System.Drawing.Point(93, 260);
            this.five_btn.Name = "five_btn";
            this.five_btn.Size = new System.Drawing.Size(75, 67);
            this.five_btn.TabIndex = 10;
            this.five_btn.Text = "5";
            this.five_btn.UseVisualStyleBackColor = false;
            this.five_btn.Click += new System.EventHandler(this.five_btn_Click);
            // 
            // four_btn
            // 
            this.four_btn.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.four_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.four_btn.Location = new System.Drawing.Point(12, 260);
            this.four_btn.Name = "four_btn";
            this.four_btn.Size = new System.Drawing.Size(75, 67);
            this.four_btn.TabIndex = 9;
            this.four_btn.Text = "4";
            this.four_btn.UseVisualStyleBackColor = false;
            this.four_btn.Click += new System.EventHandler(this.four_btn_Click);
            // 
            // three_btn
            // 
            this.three_btn.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.three_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.three_btn.Location = new System.Drawing.Point(174, 333);
            this.three_btn.Name = "three_btn";
            this.three_btn.Size = new System.Drawing.Size(75, 67);
            this.three_btn.TabIndex = 15;
            this.three_btn.Text = "3";
            this.three_btn.UseVisualStyleBackColor = false;
            this.three_btn.Click += new System.EventHandler(this.three_btn_Click);
            // 
            // two_btn
            // 
            this.two_btn.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.two_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.two_btn.Location = new System.Drawing.Point(93, 333);
            this.two_btn.Name = "two_btn";
            this.two_btn.Size = new System.Drawing.Size(75, 67);
            this.two_btn.TabIndex = 14;
            this.two_btn.Text = "2";
            this.two_btn.UseVisualStyleBackColor = false;
            this.two_btn.Click += new System.EventHandler(this.two_btn_Click);
            // 
            // one_btn
            // 
            this.one_btn.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.one_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.one_btn.Location = new System.Drawing.Point(12, 333);
            this.one_btn.Name = "one_btn";
            this.one_btn.Size = new System.Drawing.Size(75, 67);
            this.one_btn.TabIndex = 13;
            this.one_btn.Text = "1";
            this.one_btn.UseVisualStyleBackColor = false;
            this.one_btn.Click += new System.EventHandler(this.one_btn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(64, 484);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(213, 17);
            this.label1.TabIndex = 17;
            this.label1.Text = "Developed by Mehran Abghari :)";
            // 
            // zero_btn
            // 
            this.zero_btn.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.zero_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.zero_btn.Location = new System.Drawing.Point(12, 406);
            this.zero_btn.Name = "zero_btn";
            this.zero_btn.Size = new System.Drawing.Size(156, 67);
            this.zero_btn.TabIndex = 18;
            this.zero_btn.Text = "0";
            this.zero_btn.UseVisualStyleBackColor = false;
            this.zero_btn.Click += new System.EventHandler(this.zero_btn_Click);
            // 
            // dot_btn
            // 
            this.dot_btn.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.dot_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dot_btn.Location = new System.Drawing.Point(174, 406);
            this.dot_btn.Name = "dot_btn";
            this.dot_btn.Size = new System.Drawing.Size(75, 67);
            this.dot_btn.TabIndex = 19;
            this.dot_btn.Text = "\t•";
            this.dot_btn.UseVisualStyleBackColor = false;
            this.dot_btn.Click += new System.EventHandler(this.dot_btn_Click);
            // 
            // sum_btn
            // 
            this.sum_btn.BackColor = System.Drawing.Color.Lime;
            this.sum_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sum_btn.ForeColor = System.Drawing.SystemColors.ControlText;
            this.sum_btn.Location = new System.Drawing.Point(254, 187);
            this.sum_btn.Name = "sum_btn";
            this.sum_btn.Size = new System.Drawing.Size(75, 67);
            this.sum_btn.TabIndex = 20;
            this.sum_btn.Text = "+";
            this.sum_btn.UseVisualStyleBackColor = false;
            this.sum_btn.Click += new System.EventHandler(this.sum_btn_Click);
            // 
            // no1_label
            // 
            this.no1_label.AutoSize = true;
            this.no1_label.Location = new System.Drawing.Point(12, 9);
            this.no1_label.Name = "no1_label";
            this.no1_label.Size = new System.Drawing.Size(0, 17);
            this.no1_label.TabIndex = 21;
            // 
            // operator_label
            // 
            this.operator_label.AutoSize = true;
            this.operator_label.Location = new System.Drawing.Point(138, 9);
            this.operator_label.Name = "operator_label";
            this.operator_label.Size = new System.Drawing.Size(0, 17);
            this.operator_label.TabIndex = 22;
            // 
            // no2_label
            // 
            this.no2_label.AutoSize = true;
            this.no2_label.Location = new System.Drawing.Point(298, 9);
            this.no2_label.Name = "no2_label";
            this.no2_label.Size = new System.Drawing.Size(0, 17);
            this.no2_label.TabIndex = 23;
            // 
            // Calculator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(342, 511);
            this.Controls.Add(this.no2_label);
            this.Controls.Add(this.operator_label);
            this.Controls.Add(this.no1_label);
            this.Controls.Add(this.sum_btn);
            this.Controls.Add(this.dot_btn);
            this.Controls.Add(this.zero_btn);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.three_btn);
            this.Controls.Add(this.two_btn);
            this.Controls.Add(this.one_btn);
            this.Controls.Add(this.calc_btn);
            this.Controls.Add(this.six_btn);
            this.Controls.Add(this.five_btn);
            this.Controls.Add(this.four_btn);
            this.Controls.Add(this.mult_btn);
            this.Controls.Add(this.nine_btn);
            this.Controls.Add(this.eight_btn);
            this.Controls.Add(this.seven_btn);
            this.Controls.Add(this.subs_btn);
            this.Controls.Add(this.divide_btn);
            this.Controls.Add(this.clear_current_btn);
            this.Controls.Add(this.clear_btn);
            this.Controls.Add(this.input);
            this.Name = "Calculator";
            this.Text = "Calcualtor";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox input;
        private System.Windows.Forms.Button clear_btn;
        private System.Windows.Forms.Button clear_current_btn;
        private System.Windows.Forms.Button divide_btn;
        private System.Windows.Forms.Button subs_btn;
        private System.Windows.Forms.Button mult_btn;
        private System.Windows.Forms.Button nine_btn;
        private System.Windows.Forms.Button eight_btn;
        private System.Windows.Forms.Button seven_btn;
        private System.Windows.Forms.Button calc_btn;
        private System.Windows.Forms.Button six_btn;
        private System.Windows.Forms.Button five_btn;
        private System.Windows.Forms.Button four_btn;
        private System.Windows.Forms.Button three_btn;
        private System.Windows.Forms.Button two_btn;
        private System.Windows.Forms.Button one_btn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button zero_btn;
        private System.Windows.Forms.Button dot_btn;
        private System.Windows.Forms.Button sum_btn;
        private System.Windows.Forms.Label no1_label;
        private System.Windows.Forms.Label operator_label;
        private System.Windows.Forms.Label no2_label;
    }
}