﻿namespace FormApps
{
    partial class Launcher
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.calculator_btn = new System.Windows.Forms.Button();
            this.colorize_btn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.users_form_btn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // calculator_btn
            // 
            this.calculator_btn.Location = new System.Drawing.Point(12, 12);
            this.calculator_btn.Name = "calculator_btn";
            this.calculator_btn.Size = new System.Drawing.Size(294, 52);
            this.calculator_btn.TabIndex = 1;
            this.calculator_btn.Text = "Calculator";
            this.calculator_btn.UseVisualStyleBackColor = true;
            this.calculator_btn.Click += new System.EventHandler(this.advanced_calc_btn_Click);
            // 
            // colorize_btn
            // 
            this.colorize_btn.Location = new System.Drawing.Point(12, 70);
            this.colorize_btn.Name = "colorize_btn";
            this.colorize_btn.Size = new System.Drawing.Size(294, 52);
            this.colorize_btn.TabIndex = 4;
            this.colorize_btn.Text = "Colorize";
            this.colorize_btn.UseVisualStyleBackColor = true;
            this.colorize_btn.Click += new System.EventHandler(this.advanced_form_btn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(48, 198);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(213, 17);
            this.label1.TabIndex = 5;
            this.label1.Text = "Developed by Mehran Abghari :)";
            // 
            // users_form_btn
            // 
            this.users_form_btn.Location = new System.Drawing.Point(12, 128);
            this.users_form_btn.Name = "users_form_btn";
            this.users_form_btn.Size = new System.Drawing.Size(294, 52);
            this.users_form_btn.TabIndex = 6;
            this.users_form_btn.Text = "Users Form";
            this.users_form_btn.UseVisualStyleBackColor = true;
            this.users_form_btn.Click += new System.EventHandler(this.users_form_btn_Click);
            // 
            // Launcher
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(320, 234);
            this.Controls.Add(this.users_form_btn);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.colorize_btn);
            this.Controls.Add(this.calculator_btn);
            this.Name = "Launcher";
            this.Text = "Launcher";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button calculator_btn;
        private System.Windows.Forms.Button colorize_btn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button users_form_btn;
    }
}

