﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FormApps
{
    public partial class Launcher : Form
    {
        public Launcher()
        {
            InitializeComponent();
        }

        private void advanced_calc_btn_Click(object sender, EventArgs e)
        {
            (new Calculator()).Show();
        }

        private void advanced_form_btn_Click(object sender, EventArgs e)
        {
            (new Colorize()).Show();
        }

        private void users_form_btn_Click(object sender, EventArgs e)
        {
            (new UsersForm()).Show();
        }
    }
}
