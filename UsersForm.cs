﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FormApps
{
    public partial class UsersForm : Form
    {
        public UsersForm()
        {
            InitializeComponent();
        }

        private void create_btn_Click(object sender, EventArgs e)
        {
            if (first_name_input.Text.Length == 0 || last_name_input.Text.Length == 0)
            {
                MessageBox.Show("Please enter first and last name!");
                return;
            }

            users_combo.Items.Add(string.Format("{0} {1}", first_name_input.Text, last_name_input.Text));
            first_name_input.Text = "";
            last_name_input.Text = "";
        }

        private void add_to_initial_list_btn_Click(object sender, EventArgs e)
        {
            if (users_combo.Items.Count == 0)
            {
                MessageBox.Show("There isn't any items in list!");
                return;
            }
            foreach (string person in users_combo.Items)
            {
                initial_list.Items.Add(person);
            }
            users_combo.Items.Clear();
        }

        private void add_all_final_Click(object sender, EventArgs e)
        {
            if (initial_list.Items.Count == 0)
            {
                MessageBox.Show("There is not any items in list!");
                return;
            }
            foreach (string person in initial_list.Items)
            {
                final_list.Items.Add(person);
            }
            initial_list.Items.Clear();
        }

        private void add_one_final_Click(object sender, EventArgs e)
        {
            if (initial_list.Items.Count == 0)
            {
                MessageBox.Show("There is not any items in list!");
                return;
            }
            if (initial_list.SelectedIndex == -1)
            {
                MessageBox.Show("You havn't selected any item!");
                return;
            }
            string person = (string) initial_list.SelectedItem;
            final_list.Items.Add(person);
            initial_list.Items.Remove(person);
        }

        private void take_back_all_Click(object sender, EventArgs e)
        {
            if (final_list.Items.Count == 0)
            {
                MessageBox.Show("There is not any items in list!");
                return;
            }
            foreach (string person in final_list.Items)
            {
                initial_list.Items.Add(person);
            }
            final_list.Items.Clear();
        }

        private void take_back_one_Click(object sender, EventArgs e)
        {
            if (final_list.Items.Count == 0)
            {
                MessageBox.Show("There is not any items in list!");
                return;
            }
            if (final_list.SelectedIndex == -1)
            {
                MessageBox.Show("You havn't selected any item!");
                return;
            }
            string person = (string)final_list.SelectedItem;
            initial_list.Items.Add(person);
            final_list.Items.Remove(person);
        }

        private void enable_checkbox_CheckedChanged(object sender, EventArgs e)
        {
            add_all_final.Enabled = enable_checkbox.Checked;
            add_one_final.Enabled = enable_checkbox.Checked;
            take_back_all.Enabled = enable_checkbox.Checked;
            take_back_one.Enabled = enable_checkbox.Checked;
        }

        private void show_form_yes_CheckedChanged(object sender, EventArgs e)
        {
            if (show_form_yes.Checked)
            {
                final_list.Show();
            }
        }

        private void show_form_no_CheckedChanged(object sender, EventArgs e)
        {
            if (show_form_no.Checked)
            {
                final_list.Hide();
            }
        }
    }
}
