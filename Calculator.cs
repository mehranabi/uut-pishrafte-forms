﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FormApps
{
    public partial class Calculator : Form
    {
        private double no1 = -1;
        private double no2 = -1;
        private double result = -1;
        private string operat = "";

        public Calculator()
        {
            InitializeComponent();
        }

        private void input_TextChanged(object sender, EventArgs e)
        {
            if (input.Text.Length > 1 && input.Text.StartsWith("0"))
            {
                input.Text = input.Text.Substring(1);
            }
        }

        private void one_btn_Click(object sender, EventArgs e)
        {
            input.AppendText("1");
        }

        private void two_btn_Click(object sender, EventArgs e)
        {
            input.AppendText("2");
        }

        private void three_btn_Click(object sender, EventArgs e)
        {
            input.AppendText("3");
        }

        private void four_btn_Click(object sender, EventArgs e)
        {
            input.AppendText("4");
        }

        private void five_btn_Click(object sender, EventArgs e)
        {
            input.AppendText("5");
        }

        private void six_btn_Click(object sender, EventArgs e)
        {
            input.AppendText("6");
        }

        private void seven_btn_Click(object sender, EventArgs e)
        {
            input.AppendText("7");
        }

        private void eight_btn_Click(object sender, EventArgs e)
        {
            input.AppendText("8");
        }

        private void nine_btn_Click(object sender, EventArgs e)
        {
            input.AppendText("9");
        }

        private void zero_btn_Click(object sender, EventArgs e)
        {
            input.AppendText("0");
        }

        private void dot_btn_Click(object sender, EventArgs e)
        {
            input.AppendText(".");
        }

        private void clear_current_btn_Click(object sender, EventArgs e)
        {
            input.Text = "0";
        }

        private void clear_btn_Click(object sender, EventArgs e)
        {
            input.Text = "0";
            no1_label.Text = "";
            operator_label.Text = "";
            no2_label.Text = "";

            no1 = -1;
            no2 = -1;
            result = -1;
        }

        private void sum_btn_Click(object sender, EventArgs e)
        {
            if (input.Text.Length == 0)
            {
                return;
            }

            if (result != -1)
            {
                no1 = result;
                no2 = -1;
                result = -1;
                no2_label.Text = "";
            }
            else
            {
                if (no1 != -1)
                {
                    no1 += double.Parse(input.Text);
                }
                else
                {
                    no1 = double.Parse(input.Text);
                }
            }

            no1_label.Text = no1.ToString();

            operat = "+";
            operator_label.Text = operat;

            input.Text = "0";
        }

        private void subs_btn_Click(object sender, EventArgs e)
        {
            if (input.Text.Length == 0)
            {
                return;
            }

            if (result != -1)
            {
                no1 = result;
                no2 = -1;
                result = -1;
                no2_label.Text = "";
            }
            else
            {
                if (no1 != -1)
                {
                    no1 -= double.Parse(input.Text);
                }
                else
                {
                    no1 = double.Parse(input.Text);
                }
            }

            no1_label.Text = no1.ToString();

            operat = "-";
            operator_label.Text = operat;

            input.Text = "0";
        }

        private void divide_btn_Click(object sender, EventArgs e)
        {
            if (input.Text.Length == 0)
            {
                return;
            }

            if (result != -1)
            {
                no1 = result;
                no2 = -1;
                result = -1;
                no2_label.Text = "";
            }
            else
            {
                if (no1 != -1)
                {
                    double number = double.Parse(input.Text);
                    if (number != 0)
                    {
                        no1 /= number;
                    }
                }
                else
                {
                    no1 = double.Parse(input.Text);
                }
            }

            no1_label.Text = no1.ToString();

            operat = "/";
            operator_label.Text = operat;

            input.Text = "0";
        }

        private void mult_btn_Click(object sender, EventArgs e)
        {
            if (input.Text.Length == 0)
            {
                return;
            }

            if (result != -1)
            {
                no1 = result;
                no2 = -1;
                result = -1;
                no2_label.Text = "";
            }
            else
            {
                if (no1 != -1)
                {
                    no1 *= double.Parse(input.Text);
                }
                else
                {
                    no1 = double.Parse(input.Text);
                }
            }

            no1_label.Text = no1.ToString();

            operat = "*";
            operator_label.Text = operat;

            input.Text = "0";
        }

        private void clac_btn_Click(object sender, EventArgs e)
        {
            if (no1 == -1 || input.Text.Length == 0)
            {
                return;
            }

            no2 = double.Parse(input.Text);
            no2_label.Text = input.Text;

            switch (operat)
            {
                case "+":
                    result = no1 + no2;
                    break;
                case "-":
                    result = no1 - no2;
                    break;
                case "/":
                    if (no2 != 0)
                    {
                        result = no1 / no2;
                    } else
                    {
                        result = 0;
                    }
                    break;
                case "*":
                    result = no1 * no2;
                    break;
            }

            input.Text = result.ToString();
        }
    }
}
